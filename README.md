# My Dotfiles 🔧⚙

## Overview
**💻 Distro:** Manjaro\
**🍬 DE:** KDE Plasma (maybe)\
**🖥  DM:** ?\
**🔳 WM:** Qtile _(alt: i3-gaps, awesome)_\
**➖ Status bar:** _(alt: polybar)_\
**📟 Terminal:** Kitty _(Termite/Alacritty /urxvt?)_\
**⬇ Drop Term:** _(maybe also yakuake/guake)_\
**🐚 Shell:** ZSH _(oh my Zsh) (PowerLevel10k?)_\
**🎨 Color Scheme:** Nord\
**🔗 Symlink:** Stow\
**📝 Editor:** neovim\
**🌐 Internet:** Firefox\
**🗃  File explorer:** Ranger\
**📃 Document viewer:** Zathura with MuPDF\
**🎵 Spotify:** \
**📧 Email:** neomutt\
**📆 Calendar:** \
**📁 Fancy ls:** exa\
**😀 Emoji:** twemoji\
**🔤 Font:** \
**🔎 Menu:** Rofi/dmenu/


## Details
### 🐧 Linux (dotfiles) :
- \~/.profile
- \~/.xresources


### 🔳 Qtile:
- \~/qtile/config.py *(dotfile)*


### 🦊 Firefox: 
- **Nord** *(Theme)*
- **HTTPS Everywhere**
- I don't care about cookies *(Disable cookie warnings)*
- Reddit Enhancement Suite
- Stylus *(Edit css to enable dark mode)*
- Terms of service; Didn't read
- The Camelizer *(camelcamelcamel)*
- Tridactyl *(Vim)*
- uBlock origin *(Ad-Blocker)*


### 📝 Neovim:
- init.vim *(dotfile)*
- vim-plug *(Plugin manager)*
- nord-vim *(color scheme)*
- coc-nvim
- indentLine *(show indent guides)*
- Markdown-Preview.nvim *(Markdown preview in browser)*
- NERDTree *(Directory manager)*


### Oh my Zsh:
- \~/.zshrc *(dotfile)*

