# Color scheme

# Nord Color Theme
colors = ["2e3440", #Nord0    Background
        "3b4252", #Nord1    nBlack
        "434c5e", #Nord2    bnBlack
        "4c566a", #Nord3    bBlack
        "d8dee9", #Nord4    Foreground
        "e5e9f0", #Nord5    nWhite
        "eceff4", #Nord6    bWhite
        "8fbcbb", #Nord7    bCyan
        "88c0d0", #Nord8    nCyan
        "81a1c1", #Nord9    Blue
        "5e81ac", #Nord10   dBlue
        "bf616a", #Nord11   Red
        "d08770", #Nord12   Orange
        "ebcb8b", #Nord13   Yellow
        "a3be8c", #Nord14   Green
        "b48ead"] #Nord15   Magenta

