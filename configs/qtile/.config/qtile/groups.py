# Groups

# Import qtile libraries
from libqtile.config import Group, Match
# Import local files
from variables import *

groups = [
    Group(
        "1",
        label=""
    ),
    Group(
        "2",
        matches=[Match(wm_class=["firefox"])],
        label=""
    ),
    Group(
        "3",
        matches=[Match(wm_class=["Emacs"])],
        label=""
    ),
    Group(
        "4",
        matches=[Match(wm_class=["libreoffice"])],
        label=""
    ),
    Group(
        "5",
        matches=[Match(wm_class=["Thunderbird"])],
        label=""
    ),
    Group(
        "6",
        matches=[Match(wm_class=["code-oss"])],
        label=""
    ),
    Group(
        "7",
        label=""
    ),
#     Group(
#         "8"
#         label="X"
#     ),
]
