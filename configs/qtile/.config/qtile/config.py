# Qtile Config file

# Import python libraries
# import os, re, socket, subprocess, os.path
# Import qtile libraries
from libqtile.config import Key, Screen, Group, Match, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.widget import Spacer, base
from typing import List  # noqa: F401
# Import local files
from groups import groups
from keys import keys
from layouts import layouts
from floating_layout import floating_layout
from mouse import mouse
from bar import widget_defaults, extension_defaults, get_bar
from variables import *

# Activate top bar
screens = [
    Screen(
        top=get_bar(),
        wallpaper = '~/Downloads/Marmotamaps_Wallpaper_MonteViso_Desktop_1920x1080.jpg',
        wallpaper_mode = 'fill',
    ),
    Screen(),
]

# Set keybindings for groups 
for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
    ])

# Properties
dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False

auto_fullscreen = True
focus_on_window_activation = "smart"

# Don't remove
wmname = "LG3D"


# @hook.subscribe.startup_once
# def autostart():
#     home = os.path.expanduser('~/.config/qtile/autostart.sh')
#     subprocess.call([home])
