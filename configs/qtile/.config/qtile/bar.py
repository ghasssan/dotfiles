# Bar

# Import python libraries
from typing import List  # noqa: F401
# Import qtile libraries
from libqtile.command import lazy
from libqtile import bar, widget
from libqtile.widget import Spacer, base
# Import local files
from variables import *

widget_defaults = dict(
    font=FONT,
    fontsize=FONTSIZE,
    padding=4,
    background=BACKGROUND,
    foreground=FOREGROUND,
)
extension_defaults = widget_defaults.copy()

def get_bar():
    return bar.Bar([
        widget.GroupBox(
            active = BLUE_DARK,
            inactive = GRAY_LIGHT,
            this_current_screen_border = BLUE_LIGHT,
            this_screen_border = BLUE_LIGHT,
            highlight_method = "line",
            highlight_color=[BACKGROUND, BACKGROUND],
            block_highlight_text_color = BLUE_LIGHT,
            urgent_text = RED,
            urgent_border = RED,
            center_aligned=True,
            disable_drag=True,
            # Hide last group that displays as 'a' (bug)
            visible_groups = ['1','2','3','4','5','6','7'],
        ),
        widget.TextBox(
            text='|',
            foreground=BLUE_DARK,
        ),
        widget.Prompt(
            prompt='Run: ',
        ),
        # widget.TaskList(
        # )
        # widget.TaskList(
        #     foreground = BACKGROUND,
        #     border = BLUE_DARK,
        #     fontsize = 11,
        #     unfocused_border = PURPLE,
        #     highlight_method = "block",
        #     max_title_width=100,
        #     title_width_method="uniform",
        #     icon_size = 13,
        #     rounded=False,
        # ),
        widget.Spacer(bar.STRETCH),
        widget.Systray(
        ),
        widget.TextBox(
            text='|',
            foreground=BLUE_DARK,
        ),
        widget.KeyboardLayout(
            fmt = ' {}',
            configured_keyboards = ['us','ar'],
            foreground=TEAL,
        ),
        widget.TextBox(
            text='|',
            foreground=BLUE_DARK,
        ),
        widget.Battery(
            format = '{char} {percent:2.0%}',
            charge_char = '',
            discharge_char = '',
            foreground=GREEN,
            low_percentage=0.25,
            low_foreground=RED,
        ),
        widget.TextBox(
            text='|',
            foreground=BLUE_DARK,
        ),
        widget.Volume(
            fmt = ' {}',
            foreground=ORANGE,
        ),
        widget.TextBox(
            text='|',
            foreground=BLUE_DARK,
        ),
        widget.Backlight(
            format = '{percent: 2.0%}',
            foreground=YELLOW,
            backlight_name="intel_backlight",
        ),
        widget.TextBox(
            text='|',
            foreground=BLUE_DARK,
        ),
        widget.Clock(
            format=' %a %I:%M',
            foreground = TEAL,
        ),
        widget.TextBox(
            text='|',
            foreground=BLUE_DARK,
        ),
        widget.Wlan(
            foreground=PURPLE,
            interface="wlp3s0",
            format=" {essid}",
        ),
    ], 52, background=BACKGROUND)

