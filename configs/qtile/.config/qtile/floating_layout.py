# Floating layouts

# Import qtile libraries
from libqtile import layout

floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    # From Manjaro i3
    {'wname': 'alsamixer'},
    {'wmclass': 'calamares'},
    {'wmclass': 'Clipgrab'},
    {'wmclass': 'fpakman'},
    {'wmclass': 'Galculator'},
    {'wmclass': 'Gparted'},
    {'wname': 'File Transfer'},
    {'wmclass': 'Lightdm-settings'},
    {'wmclass': 'Lxappearance'},
    {'wmclass': 'Manjaro-hello'},
    {'wmclass': 'Manjaro Settings Manager'},
    {'wmclass': 'Nitrogen'},
    {'wmclass': 'Oblogout'},
    {'wmclass': 'octopi'},
    {'wmclass': 'Pamac-manager'},
    {'wmclass': 'Pavucontrol'},
    {'wmclass': 'qt5ct'},
    {'wmclass': 'Qtconfig-qt4'},
    {'wmclass': 'Simple-scan'},
    {'wmclass': 'Skype'},
    {'wmclass': 'Timeset-gui'},
    {'wmclass': 'Xfburn'},
    {'wname': 'MuseScore: Play Panel'},
])
