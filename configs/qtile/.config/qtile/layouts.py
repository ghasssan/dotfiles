# Layouts 

# Import qtile libraries
from libqtile import layout
# Import local files
from variables import *


layouts = [
    layout.MonadTall(
        border_focus = BLUE_DARK,
        border_normal = BACKGROUND,
        border_width = BORDER_WIDTH,
        margin = MARGIN,
    ),
    layout.MonadWide(
        border_focus = BLUE_DARK,
        border_normal = BACKGROUND,
        border_width = BORDER_WIDTH,
        margin = MARGIN,
    ),
]
