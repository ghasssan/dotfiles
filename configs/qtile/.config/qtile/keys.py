# Keybindings

# Import qtile libraries
from libqtile.config import Key
from libqtile.command import lazy
# Import local files
import ext
from variables import *


def init_keys():
    keys = [
        # Change Focus:
        Key([mod], "h", lazy.layout.left()),
        Key([mod], "l", lazy.layout.right()),
        Key([mod], "j", lazy.layout.down()),
        Key([mod], "k", lazy.layout.up()),
        # Swap places:
        Key([mod, "shift"], "h", lazy.layout.swap_left()),
        Key([mod, "shift"], "l", lazy.layout.swap_right()),
        Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
        Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
        # Key([mod], "w", lazy.to_screen(0)),
        # Key([mod], "y", lazy.to_screen(1)),
        # Key([mod, "shift"], "w", lazy.window.to_screen(0)),
        # Key([mod, "shift"], "y", lazy.window.to_screen(1)),

        # Resize keys:
        Key([mod], "i", lazy.layout.grow()),
        Key([mod], "m", lazy.layout.shrink()),
        Key([mod], "n", lazy.layout.normalize()),
        Key([mod], "o", lazy.layout.maximize()),
        # Move the master pane Left/Right:
        Key([mod, "shift"], "space", lazy.layout.flip()),
        # Toggel fullscreen on/off:
        Key([mod], "f", lazy.window.toggle_fullscreen()),
        # Toggel floating on/off:
        Key([mod, "control"], "f", lazy.window.toggle_floating()),

        # Change Layout:
        Key([mod], "Tab", lazy.next_layout()),
        # Close focused window:
        Key([mod], "q", lazy.window.kill()),

        # Restart qtile in place:
        Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile"),
        # Shutdown qtile:
        Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown qtile"),

        # Open a run prompt:
        Key([mod], "r", lazy.spawncmd()),

        # Applications/Scripts Shortcuts:
        Key([mod], "Return", lazy.spawn(TERMINAL)),
        Key([mod], "p", lazy.spawn("./Scripts/pmenu.sh")),
        Key([mod, "shift"], "f", lazy.spawn(BROWSER)),
        Key([mod, "shift"], "e", lazy.spawn("emacs")),
        Key([mod, "shift"], "t", lazy.spawn("thunderbird")),
        Key([mod, "shift"], "b", lazy.spawn("thunar")),
        # Key([mod], "d", lazy.spawn("dmenu")),
        Key([mod, "shift"], "d", lazy.spawn("rofi -show run")),
        Key([mod, "shift"], "p", lazy.spawn("./Scripts/pdfs.sh")),

        # Extensions
        Key([mod], 'd', ext.dmenu_run()),
        Key([mod], 'w', ext.window_list()),

        # Backlight control:
        Key([mod], "Down", lazy.spawn("light -U 5")),
        Key([mod], "Up", lazy.spawn("light -A 5")),
        Key([mod], "XF86MonBrightnessDown", lazy.spawn("light -U 5")),
        Key([mod], "XF86MonBrightnessUp", lazy.spawn("light -A 5")),

        # Volume control:
        Key([mod], "Left", lazy.spawn("amixer -c 0 -q set Master 2dB-")),
        Key([mod], "Right", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
        Key([mod], "XF86AudioLowerVolume", lazy.spawn("amixer -c 0 -q set Master 2dB-")),
        Key([mod], "XF86AudioRaiseVolume", lazy.spawn("amixer -c 0 -q set Master 2dB+")),

        # Change keyboard layout:
        Key([mod], "space", lazy.spawn("./Scripts/kbdlayout.sh")),
    ]
    return keys

keys = init_keys()
