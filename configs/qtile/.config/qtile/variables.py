# Set default variables

# Import local files
from colors import colors


# Set mod key
mod = "mod4"                # modkey

# Set properties
FONT = 'FiraGO'
FONTSIZE = 40

# Layouts
MARGIN = 4
BORDER_WIDTH = 2

# Set Applications
TERMINAL = "kitty"          # Terminal emulator
BROWSER = "firefox"         # Internet Browser
EDITOR = "neovim"           # Text Editor
EXPLORER = "ranger"         # File Explorer
EMAIL_GUI = "thunderbird"   # Email client (Graphical)

## Colors
# Blacks/Grays
BACKGROUND = colors[0]
GRAY_DARK = colors[1]
GRAY_MEDIUM = colors[2]
GRAY_LIGHT = colors[3]
# Whites
FOREGROUND = colors[4]
WHITE_MEDIUM = colors[5]
WHITE_LIGHT = colors[6]
# Blues
TEAL = colors[7]
BLUE_LIGHT = colors[8]
BLUE_MEDIUM = colors[9]
BLUE_DARK = colors[10]
# Other colors
RED = colors[11]
ORANGE = colors[12]
YELLOW = colors[13]
GREEN = colors[14]
PURPLE = colors[15]
