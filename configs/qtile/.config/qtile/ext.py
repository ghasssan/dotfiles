# Extensions

# Import python libraries
from libqtile.command import lazy
from libqtile import extension
# Import local files
from variables import *


extension_defaults = dict(
    font=FONT,
    fontsize=FONTSIZE,
    padding=4,
    background=BACKGROUND,
    foreground=FOREGROUND,
)

def dmenu_run():
    return lazy.run_extension(extension.DmenuRun(
        dmenu_prompt=">",
        # dmenu_font="Andika-8",
        fontsize=12,
        background="#" + BACKGROUND,
        foreground="#" + FOREGROUND,
        selected_background="#" + BLUE_DARK,
        selected_foreground="#" + BACKGROUND,
        # dmenu_height=58,  # Only supported by some dmenu forks
        dmenu_ignorecase=True
    ))

def window_list():
    return lazy.run_extension(extension.WindowList(
        dmenu_prompt=">",
        # dmenu_font="Andika-8",
        fontsize=12,
        background="#" + BACKGROUND,
        foreground="#" + FOREGROUND,
        selected_background="#" + BLUE_DARK,
        selected_foreground="#" + BACKGROUND,
        # dmenu_height=58,  # Only supported by some dmenu forks
        dmenu_ignorecase=True
    ))
