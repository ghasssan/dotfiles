# My Configs 🔧

**dir\colors:** Nord theme for `ls` and `tree`\
**dmenu:** Window launcher menu\
**gtk:** Config files for gtk 2.0 & gtk 3.0
**📝 neovim:** Editor based on vim\
**qtile:** Windows Manager configured in python\
**⚙ Xresources:** Config files for Xresources

