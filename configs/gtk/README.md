# GTK Settings

### GTK 2.0
- ~/.gtkrc-2.0

### GTK 3.0
- ~/.config/gtk-3.0/settings.ini
- ~/.config/gtk-3.0/gtk.css
