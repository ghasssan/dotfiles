# Xresources

- .Xresources
- .config/Xresources/
    - nord (theme)
    - fonts (font and cursor settings)
    - dpi (create this file to fix scaling issues)



### TODO: 
1. Create file named "dpi" in "~/.config/Xresources"
2. Add the following line to the file:\

``Xft.dpi: 96``\

3. Adjust the number depending on your display
4. Add "dpi" file to gitignore to avoid syncing 
