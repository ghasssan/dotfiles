# 📝 Neovim:
- init.vim *(config file)*
    - Unix: *\~/.config/nvim/init.vim*
    - Windows: *\~/AppData/Local/nvim/init.vim*
- vim-plug *(Plugin manager)*
- nord-vim *(color scheme)*
- coc-nvim
- indentLine *(show indent guides)*
- Markdown-Preview.nvim *(Markdown preview in browser)*
- NERDTree *(Directory manager)*
