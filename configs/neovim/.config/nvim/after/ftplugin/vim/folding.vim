function! VimFolds()
    let thisline = getline(v:lnum)
    
    if match(thisline, '^"" ') >= 0
        return ">1"
    elseif match(thisline, '^""" ') >= 0
        return ">2"
    elseif match(thisline, '^"""" ') >= 0
        return ">3"
    elseif match(thisline, '^""""" ') >= 0
        return ">4"
    elseif match(thisline, '^"""""" ') >= 0
        return ">5"
    else
        return "="
    endif
endfunction
setlocal foldmethod=expr
setlocal foldexpr=VimFolds()


function! VimFoldText()
    let foldsize = (v:foldend-v:foldstart)
    return getline(v:foldstart).' ('.foldsize.' lines)'
endfunction
setlocal foldtext=VimFoldText()
